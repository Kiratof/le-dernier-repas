class Restaurant {
    constructor({ id, name, address, location, ratings }) {
        this.id = id
        this.name = name
        this.address = address
        this.location = location

        this.photo = null
        this.ratings = ratings
        this.placeMarkerOnMap()
    }

    placeMarkerOnMap() {
        addMarker({
            location: this.location
        })
    }

    addratings(rating) {
        this.ratings.push(rating)
    }

    getAverageRate() {
        let average = 0
        let sum = 0
        let ratings = this.ratings

        for (var i = 0; i < ratings.length; i++) {
            sum += parseInt(ratings[i].stars);
        }

        average = sum / ratings.length

        return average
    }

    getTotalRatings() {
        return this.ratings.length
    }

    isInMapBounds(mapBounds) {

        let isInMapBounds = false

        let South_Lat = mapBounds.getSouthWest().lat();
        let South_Lng = mapBounds.getSouthWest().lng();
        let North_Lat = mapBounds.getNorthEast().lat();
        let North_Lng = mapBounds.getNorthEast().lng();

        if (this.location.lng > South_Lng && this.location.lng < North_Lng && this.location.lat > South_Lat && this.location.lat < North_Lat) {
            isInMapBounds = true;
        }

        return isInMapBounds
    }

    getLocation() {
        return this.location
    }

    renderCardHtmlContent() {
        //Création des éléments HTML
        let restaurantContainer = document.getElementById('restaurants')
        let cardElement = document.createElement('div')
        let infoContainer = document.createElement('div')
        let headingElement = document.createElement('h2')
        let ratingElement = document.createElement('div')
        let addressElement = document.createElement('p')
        let btnElement = document.createElement('button')

        //Ajout des classes
        cardElement.className = "card"
        infoContainer.className = "restaurant"
        headingElement.className = "restaurant__name"
        ratingElement.className = "restaurant__rating"
        addressElement.className = "resturant__address"
        btnElement.className = "restaurant__button"

        //EVENEMENT AFFICHAGE DU DETAIL
        btnElement.addEventListener("click", function (event) {
            let sideNav = document.getElementById('sidenav')
            let panelDetails = document.getElementById('locations-panel-details')



            //RENDER DETAILS
            //Data du restaurant
            const restaurant = restaurantList[event.target.dataset.id]

            //CENTRAGE CAMERA
            const pos = {
                lat: restaurant.location.lat,
                lng: restaurant.location.lng,
            }
            recenterCameraOnUserPosition(pos)


            //Ajout des informations générale du restaurant
            let restaurantDetailsContainer = document.createElement('div')
            let photoDetailsContainer = document.createElement('div')
            let photoDetailsElement = document.createElement('img')
            let nameDetailsElement = document.createElement('div')
            let addressDetailsElement = document.createElement('div')
            let ratingsDetailsElement = document.createElement('div')

            //Ajout des classes
            restaurantDetailsContainer.className = 'restaurantDetails'
            photoDetailsElement.className = 'restaurantDetails__photo'
            nameDetailsElement.className = 'restaurantDetails__name'
            addressDetailsElement.className = 'restaurantDetails__address'
            ratingsDetailsElement.className = 'restaurantDetails__ratings'


            //Ajout du contenu

            //GOOGLE STREET VIEW CALL
            let location = restaurant.location.lat + ',' + restaurant.location.lng
            photoDetailsElement.src = 'https://maps.googleapis.com/maps/api/streetview?size=600x300&location=' + location + '&heading=151.78&pitch=-0.76&key=AIzaSyA6EvOMoUa6hageaOQOZkGtzqGk8Xbx0o8';
            nameDetailsElement.innerText = restaurant.name
            addressDetailsElement.innerText = restaurant.address
            ratingsDetailsElement.innerHTML = '<h3>Avis ' + ' (' + restaurant.getTotalRatings() + ')' + ' </h3>'

            //Ajout des commentaires + note
            restaurant.ratings.map((rating) => {
                //Création des éléments
                let ratingElement = document.createElement('div')
                let commentElement = document.createElement('div')
                let starsElement = document.createElement('div')

                //Ajout des classes
                ratingElement.className = 'rating'
                commentElement.className = 'rating__comment'
                starsElement.className = 'rating__stars'

                //Ajout de la note
                starsElement.innerText = restaurant.getAverageRate()
                let numberOfStars = Math.floor(restaurant.getAverageRate())
                for (let i = 0; i < numberOfStars; i++) {
                    addStar(starsElement, "star")
                }
                if (numberOfStars < restaurant.getAverageRate()) {
                    addStar(starsElement, "star star--half")

                }

                //Ajout du commentaire
                commentElement.innerText = rating.comment

                //Imbrication
                ratingElement.append(starsElement)
                ratingElement.append(commentElement)
                ratingsDetailsElement.append(ratingElement)
            })

            //Imbrication des éléments
            photoDetailsContainer.append(photoDetailsElement)
            restaurantDetailsContainer.append(photoDetailsContainer, nameDetailsElement, addressDetailsElement, ratingsDetailsElement)
            panelDetails.append(restaurantDetailsContainer)

            hideElement(sideNav)
            showElement(panelDetails)
        })

        //Ajout des "meta donnees"
        btnElement.dataset.id = this.id

        //Ajout du contenu
        headingElement.innerText = this.name
        addressElement.innerText = this.address
        ratingElement.innerText = this.getAverageRate()
        let numberOfStars = Math.floor(this.getAverageRate())
        for (let i = 0; i < numberOfStars; i++) {
            addStar(ratingElement, "star")
        }
        if (numberOfStars < this.getAverageRate()) {
            addStar(ratingElement, "star star--half")

        }
        ratingElement.append(' (' + this.getTotalRatings() + ')')
        btnElement.innerText = "Voir détails"

        //Imbrication des éléments
        restaurantContainer.append(cardElement)
        cardElement.append(infoContainer)
        infoContainer.append(headingElement, ratingElement, addressElement, btnElement)
    }

}

//Ce qui pourrait manquer d'une ecole dinge
//Organisation
//Demarche
//Connaissance approfondi dans certains domaines (math, physique, autre)