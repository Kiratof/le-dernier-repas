let map, infoWindow
let restaurantList = []
let lowRate = 0
let highRate = 5

function runApp() {
    initMap()
    initUserPosition()
    addRestaurants()
}

function initMap() {
    //Map option
    let options = {
        center: { lat: 43.583328, lng: 3.8 },
        zoom: 10
    }

    createMap(options)
    addMapEventListener()
}

function createMap(options) {
    map = new google.maps.Map(document.getElementById("map"), options)
}

function addMapEventListener() {
    google.maps.event.addListener(map, 'bounds_changed', function () {
        updateRestaurantList()
    })

    google.maps.event.addListener(map, "click", (event) => {
        addMarker({ location: event.latLng })
    })
}

function updateRestaurantList() {

    const cloneRestaurantList = [...restaurantList];

    //Vide la liste
    let restaurants = document.getElementById('restaurants')
    while (restaurants.firstChild) {
        restaurants.removeChild(restaurants.firstChild)
    }

    //Filtre sur la note moyenne
    let filterCloneRestaurant = cloneRestaurantList.filter((rest) => {
        return rest.getAverageRate() <= highRate && rest.getAverageRate() >= lowRate
    })

    //Ajout des restaurants visibles
    let mapBounds = map.getBounds()

    filterCloneRestaurant.forEach(restaurant => {
        if (restaurant.isInMapBounds(mapBounds)) {
            addRestaurantToTheSideNav(restaurant)
        }
    })
}

function addRestaurantToTheSideNav(restaurant) {
    createCard(restaurant)
}

function createCard(restaurant) {
    restaurant.renderCardHtmlContent()
}

function addStar(element, type) {
    let starElement = document.createElement('span')
    starElement.className = type

    element.append(starElement)
}

function initUserPosition() {
    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        getUserCurrentPosition()
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter())
    }
}

function getUserCurrentPosition() {
    navigator.geolocation.getCurrentPosition(
        (position) => {
            const pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude,
            }
            recenterCameraOnUserPosition(pos)
            addUserMarker(pos)
        }
    )
}

function addUserMarker(pos) {
    //Creation Icon
    const urlIcon = "https://image.flaticon.com/icons/png/512/16/16363.png"
    const iconSize = new google.maps.Size(50, 50)
    const icon = {
        url: urlIcon,
        scaledSize: iconSize
    }

    //Add marker
    addMarker({
        location: pos,
        icon: icon
    })
}

function recenterCameraOnUserPosition(pos) {
    map.setCenter(pos)
    map.setZoom(15)
}


function addRestaurants() {
    let sideNav = document.getElementById('sidenav')
    let panelDetails = document.getElementById('locations-panel-details')
    showElement(sideNav)
    hideElement(panelDetails)
    fetch("./js/restaurant.json")
        .then(function (res) {
            if (res.ok) {
                return res.json()
            }
        })
        .then(function (value) {
            buildRestaurants(value)
            updateRestaurantList()
        })
        .catch(function (err) {
            // Une erreur est survenue
        })
}

function buildRestaurants(restaurants) {
    for (let i = 0; i < restaurants.length; i++) {
        const restaurant = new Restaurant({
            id: i,
            name: restaurants[i].name,
            address: restaurants[i].address,
            location: { lat: restaurants[i].lat, lng: restaurants[i].long },
            ratings: restaurants[i].ratings
        })
        restaurantList.push(restaurant)
    }
}

function addMarker(property) {
    //Marker
    const marker = new google.maps.Marker({
        position: property.location,
        map: map,
        icon: property.icon
    })

    if (property.content) {
        //Infowindow
        const infowindow = new google.maps.InfoWindow({
            content: property.content,
        })

        marker.addListener("click", () => {
            infowindow.open(map, marker)
        })
    }
}

/** Hide a DOM element. */
function hideElement(el) {
    el.style.display = 'none';
}

/** Show a DOM element that has been hidden. */
function showElement(el) {
    el.style.display = 'block';
}